from subprocess import check_call, call


def install():
    """
    Run pip install with specified directory and bin dir.
    """
    call(['pip', 'install', 'requests', '-tpython_packages', '--install-option=--install-scripts=$PWD/bin'])

