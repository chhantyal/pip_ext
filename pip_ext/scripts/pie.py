# coding: utf-8
from __future__ import (absolute_import, print_function, unicode_literals)

import os
import sys

from .. import click, ext
from ..logging import log
from ..utils import assert_compatible_pip_version

# Make sure we're using a compatible version of pip
assert_compatible_pip_version()

DEFAULT_REQUIREMENTS_FILE = 'requirements.txt'


@click.command()
@click.version_option()
@click.argument('install', required=True, nargs=-1)
def cli(install):
    if os.path.exists(DEFAULT_REQUIREMENTS_FILE):
        pass
    else:
        msg = 'No requirement files given and no {} found in the current directory'
        log.error(msg.format(DEFAULT_REQUIREMENTS_FILE))
        sys.exit(2)

    sys.exit(ext.install())
