# coding: utf-8
from __future__ import (absolute_import, print_function, unicode_literals)

import sys

import pip


def safeint(s):
    try:
        return int(s)
    except ValueError:
        return 0

pip_version_info = tuple(safeint(digit) for digit in pip.__version__.split('.'))


def assert_compatible_pip_version():
    # Make sure we're using a reasonably modern version of pip
    if not pip_version_info >= (7, 0):
        print('pip_ext requires at least version 7.0 of pip ({} found), '
              'perhaps run `pip install --upgrade pip`?'.format(pip.__version__))
        sys.exit(4)
