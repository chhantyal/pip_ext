"""
pie = pip extension : make pip like npm, virtualenv is not required.
"""
from setuptools import find_packages, setup

setup(
    name='pip_ext',
    version='0.1',
    url='https://github.com/chhantyal/pip_ext/',
    license='BSD',
    author='Nar Kumar Chhantyal',
    author_email='nkchhantyal@gmail.com',
    description=__doc__,
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'click>=6',
        'six',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'pie = pip_ext.scripts.pie:cli',
        ],
    },
    platforms='any',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Topic :: System :: Systems Administration',
    ]
)
