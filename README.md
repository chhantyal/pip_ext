
pie = pip extension
===================

`pie` is small `pip` extension.
It makes it easy to specify where to install third party packages. 
Thus you can install project dependencies locally in single directory, without using virtualenv.  
It is similar to how `npm` install works.

By default, it installs packages in `python_packages` and scripts in `bin` in current directory.
